package com.example.bettersleep;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	Time startTime;
	Time[] offsetTimes = new Time[6];
	TextView[] textViews = new TextView[6];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startTime = new Time(Time.getCurrentTimezone());
		
		for (int i=0; i<offsetTimes.length; i++){
			offsetTimes[i] = new Time();
			offsetTimes[i].set(00, (i+1)*90, 0, 1, 1, 2014);
		}
			
		setContentView(R.layout.activity_main);
		getTextViews();
		updateTime();
	}
	
	private void getTextViews(){
		textViews[0]=(TextView)findViewById(R.id.time1);
		textViews[1]=(TextView)findViewById(R.id.time2);
		textViews[2]=(TextView)findViewById(R.id.time3);
		textViews[3]=(TextView)findViewById(R.id.time4);
		textViews[4]=(TextView)findViewById(R.id.time5);
		textViews[5]=(TextView)findViewById(R.id.time6);
	}
	
	public void setAlarm(View view){
		Time alarmTime = new Time(startTime);
		int id;
		switch (view.getId()){
		case (R.id.time1):
			id=0;
			break;
		case (R.id.time2):
			id=1;
			break;
		case (R.id.time3):
			id=2;
			break;
		case (R.id.time4):
			id=3;
			break;
		case (R.id.time5):
			id=4;
			break;
		default:
			id=5;
			break;
		}
		alarmTime.hour += offsetTimes[id].hour;
		alarmTime.minute += offsetTimes[id].minute;
		alarmTime.normalize(true);
		Toast.makeText(this, "Alarm set to " + timeToString(startTime, offsetTimes[id]), Toast.LENGTH_SHORT).show();
	}
	
	public void updateTime(View view){
		updateTime();
	}

	private void updateTime(){
		startTime.setToNow();
		startTime.minute += 14;
		startTime.normalize(true);
		
		for (int i=0;i<textViews.length; i++){
			textViews[i].setText(timeToString(startTime, offsetTimes[i]));
		}
	}
	
	private String timeToString(Time startTime, Time offset){
		int hour = startTime.hour + offset.hour;
		int minute = startTime.minute + offset.minute;
		
		while (minute >= 60){
			minute -= 60;
			hour++;
		}
		
		if (hour >= 24)
			hour -= 24;
	
		String answer = "";
		
		if (hour < 10)
			answer += "0";
		
		answer += hour + ":";
		
		if (minute < 10)
			answer += "0";
		
		answer += minute;
		return answer;
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
